#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <set>
#include <fstream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/regex.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/predicate.hpp>

// js: $("form input[type='text']").each(function(){if($(this).css('display')!='none'){this.value='%s';}});$("form input[type='password']").attr('value','%s');$('form').submit();)
// js: $("form input[type='password']").each(function(){ this.value='123'; })
// const char *jsfmt = "js: $('%s').attr('value', '%s'); $('%s').attr('value', '%s'); form_submit=$('%s').submit()\n";
const char *jsfmt = "js:"
    "$(\"form input[type='text']\").each(function(){if($(this).css('display')!='none'&&!this.value){this.value='%s';}});"
    "$(\"form input[type='password']\").attr('value','%s');"
    "$('form:submit').length?$('form:submit').click():$('form').submit();";
// const char *jsfmt = "js:$(\"form input[type='text']\").each(function(){if($(this).css('display')!='none'&&!this.value){this.value='%s';}});$(\"form input[type='password']\").attr('value','%s');$('form:submit').click();";

using namespace std;
using namespace boost;

int main(int argc, char *const argv[])
{
    if (argc < 2)
        return 1;
    if (argc > 3)
        jsfmt = argv[3];

    // string sel_user, sel_password, sel_form;

    // string line;
    // ifstream ins(argv[2]);

    // while (getline(ins, line)) {
    //     auto it = find_if(line.begin(), line.end(), !algorithm::is_space());
    //     if (it == line.end())
    //         break;
    //     if (*it == '#')
    //         continue;
    //     it = find(line.begin(), line.end(), '=');
    //     if (it == line.end())
    //         continue;

    //     string k(line.begin(), it);
    //     string w(it+1, line.end());

    //     algorithm::trim(k);
    //     algorithm::trim(w);

    //     if (algorithm::starts_with(k, "sel_user"))
    //         sel_user = w;
    //     else if (algorithm::starts_with(k, "sel_password"))
    //         sel_password = w;
    //     else if (algorithm::starts_with(k, "sel_form"))
    //         sel_form = w;
    // }

    // if (sel_user.empty() || sel_password.empty() || sel_form.empty())
    // {
    //     return 2;
    // }

    // format fmt1 = format(jsfmt) % sel_user % sel_password % sel_form;

    ofstream outs(argv[1], ios::app);
    ifstream ins(argv[2]);

    string line;
    while (getline(ins, line)) {
        auto it = find_if(line.begin(), line.end(), !algorithm::is_space());
        if (it == line.end())
            continue;
        if (*it == '#')
            continue;

        algorithm::trim(line);
        it = find(line.begin(), line.end(), ' ');
        if (it == line.end())
            continue;

        string user(line.begin(), it);
        string password(it+1, line.end());

        algorithm::trim(user);
        algorithm::trim(password);

        // outs << format(jsfmt) % sel_user % user % sel_password % password % sel_form;
        outs << format(jsfmt) % user % password << "\n";
    }

    return 0;
}

