#include <QtGui>
#include "mainwindow.h"

int main(int argc, char * argv[])
{
    QApplication app(argc, argv);
    QUrl url;
    if (argc > 1)
        url = QUrl(argv[1]);
    else
        url = QUrl("file:///tmp/test.html"); // url = QUrl("http://www.google.com/ncr");
    MainWindow *browser = new MainWindow(url);
    browser->resize(1080, 640);
    // #if defined Q_OS_SYMBIAN || defined Q_WS_HILDON || defined Q_WS_MAEMO_5 || defined Q_WS_SIMULATOR
        //browser->showMaximized();
    browser->showMinimized();
    // browser->show();
        // browser->hide();
    // #else
        // browser->show();
    // #endif
    return app.exec();
}
