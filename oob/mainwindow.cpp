#include <QtGui>
#include <QtNetwork>
#include <QtWebKit>
#include "mainwindow.h"

struct NPage : QWebPage
{
    NPage(QObject *p) : QWebPage(p) {}
    QString userAgentForUrl ( const QUrl & url ) const;
};

QString NPage::userAgentForUrl ( const QUrl & url ) const
{
    (void)url;
    // qDebug() << url;
    return "Mozilla/5.0 (Windows NT 6.1; rv:5.0) Gecko/20100101 Firefox/5.02011-10-16 20:21:42";
    // "Mozilla/5.0 (Windows NT 6.1; rv:5.0) Gecko/20100101 Firefox/5.02011-10-16 20:21:42";
    //
    // return "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11";
    //android// "Mozilla/5.0 (Linux; U; Android 2.3.3; en-au; GT-I9100 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.12011-10-16 20:22:55";
    // "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6) AppleWebKit/531.4 (KHTML, like Gecko) Version/4.0.3 Safari/531.4";
    // "Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.7 (KHTML, like Gecko) Version/5.0 Safari/534.7";
    //firefox// "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0";
    //chromium// "User-Agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/536.11 (KHTML, like Gecko) Ubuntu/12.04 Chromium/20.0.1132.47 Chrome/20.0.1132.47 Safari/536.11"
    //chrome// "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11"
    //ie9// "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
    //ie8// "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Win32)"
}


//! [1]
void MainWindow::reConnectSocket()
{
    tcpSocket->abort();
    tcpSocket->connectToHost(QHostAddress::LocalHost,9090);
}

MainWindow::MainWindow(const QUrl& url)
{
    progress = 0;
    url_ = url;

    timer_ = new QTimer(this);
    timer_->setSingleShot(true);

    QNetworkProxyFactory::setUseSystemConfiguration(true);

    tcpSocket = new QTcpSocket(this);

    connect(tcpSocket, SIGNAL(connected()), this, SLOT(reOpenLink()));
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readSocket()));
    // connect(tcpSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(updateClientProgress(qint64)));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(tcpSocketError(QAbstractSocket::SocketError)));

    reConnectSocket();

    {
        QFile file;
        file.setFileName(":/jquery.min.js");
        file.open(QIODevice::ReadOnly);
        jQuery = file.readAll();
        file.close();
    }

    view = new QWebView(this);

    connect(view, SIGNAL(loadStarted()), SLOT(startLoading()));
    connect(view, SIGNAL(loadProgress(int)), SLOT(setProgress(int)));
    connect(view, SIGNAL(loadFinished(bool)), SLOT(adjustLocation()));
    connect(view, SIGNAL(loadFinished(bool)), SLOT(finishLoading(bool)));
    connect(view, SIGNAL(titleChanged(QString)), SLOT(adjustTitle()));

    view->setPage(new NPage(view));
    view->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    connect(view->page(), SIGNAL(linkClicked(const QUrl&)), this, SLOT(openLink(const QUrl&)));
    // qDebug() << view->page()->linkDelegationPolicy() << QWebPage::DelegateAllLinks;
    // qDebug() << view->page()->action(QWebPage::OpenLink)->isEnabled();

    // view->setUrl(url);
    // view->load(url_);

    locationEdit = new QLineEdit(this);
    locationEdit->setSizePolicy(QSizePolicy::Expanding, locationEdit->sizePolicy().verticalPolicy());
    connect(locationEdit, SIGNAL(returnPressed()), SLOT(changeLocation()));

    QToolBar *toolBar = addToolBar(tr("Navigation"));
    toolBar->addAction(view->pageAction(QWebPage::Back));
    toolBar->addAction(view->pageAction(QWebPage::Forward));
    toolBar->addAction(view->pageAction(QWebPage::Reload));
    toolBar->addAction(view->pageAction(QWebPage::Stop));
    toolBar->addWidget(locationEdit);

//! [2]

    // QMenu *viewMenu = menuBar()->addMenu(tr("&View"));
    // QAction* viewSourceAction = new QAction("Page Source", this);
    // connect(viewSourceAction, SIGNAL(triggered()), SLOT(viewSource()));
    // viewMenu->addAction(viewSourceAction);

//! [3]
    // QMenu *effectMenu = menuBar()->addMenu(tr("&Effect"));
    // effectMenu->addAction("Highlight all links", this, SLOT(highlightAllLinks()));

    // rotateAction = new QAction(this);
    // rotateAction->setIcon(style()->standardIcon(QStyle::SP_FileDialogDetailedView));
    // rotateAction->setCheckable(true);
    // rotateAction->setText(tr("Turn images upside down"));
    // connect(rotateAction, SIGNAL(toggled(bool)), this, SLOT(rotateImages(bool)));
    // effectMenu->addAction(rotateAction);

    // QMenu *toolsMenu = menuBar()->addMenu(tr("&Tools"));
    // toolsMenu->addAction(tr("Remove GIF images"), this, SLOT(removeGifImages()));
    // toolsMenu->addAction(tr("Remove all inline frames"), this, SLOT(removeInlineFrames()));
    // toolsMenu->addAction(tr("Remove all object elements"), this, SLOT(removeObjectElements()));
    // toolsMenu->addAction(tr("Remove all embedded elements"), this, SLOT(removeEmbeddedElements()));

    setCentralWidget(view);
    setUnifiedTitleAndToolBarOnMac(true);
}

void MainWindow::openLink(const QUrl& url)
{
    qDebug() << url << "clicked";
    view->stop();
    view->load(url_ = url);

    // connect(timer_, SIGNAL(timeout()), this, SLOT(reOpenLink()));
    // timer_->start(3000);
}

void MainWindow::reOpenLink()
{
    // const QUrl& url = view->page()->mainFrame()->requestedUrl();
    openLink(url_);
}

void MainWindow::viewSource()
{
    QNetworkAccessManager* accessManager = view->page()->networkAccessManager();
    QNetworkRequest request(view->url());
    QNetworkReply* reply = accessManager->get(request);
    connect(reply, SIGNAL(finished()), this, SLOT(slotSourceDownloaded()));
}

void MainWindow::slotSourceDownloaded()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(const_cast<QObject*>(sender()));
    QTextEdit* textEdit = new QTextEdit(NULL);
    textEdit->setAttribute(Qt::WA_DeleteOnClose);
    textEdit->show();
    textEdit->setPlainText(reply->readAll());
    reply->deleteLater();
}

//! [4]
void MainWindow::adjustLocation()
{
    locationEdit->setText(view->url().toString());
}

void MainWindow::changeLocation()
{
    QString url = locationEdit->text();

    xinput(url);

    // if (url.startsWith("js:"))
    // {
    //     QString code = url.section(':', 1);
    //     if (!code.isEmpty()) {
    //         qDebug() << code << "\n";
    //         view->page()->mainFrame()->evaluateJavaScript(code);
    //     }
    //     return;
    // }
    // if (url.startsWith("jsf:"))
    // {
    //     QString fn = url.section(':', 1);
    //     QFile file;
    //     file.setFileName(fn);
    //     file.open(QIODevice::ReadOnly);
    //     QString code = file.readAll();
    //     file.close();
    //     if (!code.isEmpty()) {
    //         qDebug() << code << "\n";
    //         view->page()->mainFrame()->evaluateJavaScript(code);
    //     }
    //     return;
    // }

    // view->load(QUrl(url)); // view->load(url.startsWith("file:") ? QUrl::fromLocalFile(url) : QUrl(url));
    view->setFocus();
}
//! [4]

//! [5]
void MainWindow::adjustTitle()
{
    if (progress <= 0 || progress >= 100)
        setWindowTitle(view->title());
    else
        setWindowTitle(QString("%1 (%2%)").arg(view->title()).arg(progress));
}

void MainWindow::setProgress(int p)
{
    progress = p;
    adjustTitle();
}

void MainWindow::startLoading()
{
    const QUrl& url = view->page()->mainFrame()->requestedUrl();
    qDebug() << "startLoading" << view->url() << url;

    // const QString& url = view->url().toString();
    QByteArray buf;

    buf.append( url.toString() );
    buf.append( " loading\n" );

    tcpSocket->write(buf.data(), buf.length());

    // QString buf = view->url().toString() + " loading\n";
    // tcpSocket->write(buf.data(), buf.length());
}

void MainWindow::finishLoading(bool result)
{
    const QUrl& url = view->url();
    // timer_->stop();

    qDebug() << "finishLoading" << url << result << view->page()->linkDelegationPolicy() << QWebPage::DelegateAllLinks;

    if (!result) {
        QByteArray buf;
        buf.append( url.toString() ); // buf.append( view->url().toString() );
        buf.append( " fail\n" );
        tcpSocket->write(buf.data(), buf.length());
        return ;
    }

    progress = 100;
    adjustTitle();
    view->page()->mainFrame()->evaluateJavaScript(jQuery);

    // rotateImages(rotateAction->isChecked());

    // QString code = QString("alert('") + view->url().toString() + "');";
    // view->page()->mainFrame()->evaluateJavaScript(code);

    QByteArray buf;
    buf.append( url.toString() ); // buf.append( view->url().toString() );
    buf.append( " ready\n" );

    tcpSocket->write(buf.data(), buf.length());

//    QByteArray block(buf.data(), buf.length());
//    QDataStream out(&block, QIODevice::WriteOnly);
//    out.setVersion(QDataStream::Qt_4_0);
////! [4] //! [6]
//    out << (quint16)0;
//    out << fortunes.at(qrand() % fortunes.size());
//    out.device()->seek(0);
//    out << (quint16)(block.size() - sizeof(quint16));
////! [6] //! [7]
//
//    QTcpSocket *clientConnection = tcpServer->nextPendingConnection();
//    connect(clientConnection, SIGNAL(disconnected()),
//            clientConnection, SLOT(deleteLater()));
////! [7] //! [8]
//
//    clientConnection->write(block);
//    clientConnection->disconnectFromHost();
}
//! [6]

//! [7]
void MainWindow::highlightAllLinks()
{
    QString code = "$('a').each( function () { $(this).css('background-color', 'yellow') } )";
    view->page()->mainFrame()->evaluateJavaScript(code);
}
//! [7]

//! [8]
void MainWindow::rotateImages(bool invert)
{
    QString code;
    if (invert)
        code = "$('img').each( function () { $(this).css('-webkit-transition', '-webkit-transform 2s'); $(this).css('-webkit-transform', 'rotate(180deg)') } )";
    else
        code = "$('img').each( function () { $(this).css('-webkit-transition', '-webkit-transform 2s'); $(this).css('-webkit-transform', 'rotate(0deg)') } )";
    view->page()->mainFrame()->evaluateJavaScript(code);
}
//! [8]

//! [9]
void MainWindow::removeGifImages()
{
    QString code = "$('[src*=gif]').remove()";
    view->page()->mainFrame()->evaluateJavaScript(code);
}

void MainWindow::removeInlineFrames()
{
    QString code = "$('iframe').remove()";
    view->page()->mainFrame()->evaluateJavaScript(code);
}

void MainWindow::removeObjectElements()
{
    QString code = "$('object').remove()";
    view->page()->mainFrame()->evaluateJavaScript(code);
}

void MainWindow::removeEmbeddedElements()
{
    QString code = "$('embed').remove()";
    view->page()->mainFrame()->evaluateJavaScript(code);
}
//! [9]

void MainWindow::tcpSocketError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        // break;
    case QAbstractSocket::ConnectionRefusedError:
        connect(timer_, SIGNAL(timeout()), this, SLOT(reConnectSocket()));
        timer_->start(500);
        // QMessageBox::information(this, tr("Socket error"), tr("The connection was refused by the peer. " "Make sure the fortune server is running, " "and check that the host name and port " "settings are correct."));
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Socket error"),
                                 tr("The host was not found. Please check the " "host name and port settings."));
        break;
    default:
        QMessageBox::information(this, tr("Socket error"),
                                 tr("The following error occurred: %1.").arg(tcpSocket->errorString()));
    }
}

void MainWindow::readSocket()
{
    // qDebug() << "readSocket:" << tcpSocket->bytesAvailable();

    QByteArray data = tcpSocket->readAll();  
    // qDebug() << data;
    readbuf += QVariant(data).toString();  

    // QDataStream in(tcpSocket);
    // in.setVersion(QDataStream::Qt_4_0);

    // QString sbuf;
    // in >> sbuf;
    // qDebug() << "readSocket:" << tcpSocket->bytesAvailable() << ":" << sbuf;

    int x;

    while ( (x = readbuf.indexOf('\n', 0)) >= 0)
    {
        QString code;
        code.append(readbuf.midRef(0, x));

        xinput(code);
        readbuf.remove(0, x+1);
    }

    // if (blockSize == 0) {
    //     if (tcpSocket->bytesAvailable() < (int)sizeof(quint16))
    //         return;
    //     in >> blockSize;
    // }

    // if (tcpSocket->bytesAvailable() < blockSize)
    //     return;
// line.indexOf(QRegExp("\\s"), pb);

    // QString nextFortune;
    // in >> nextFortune;

    // if (nextFortune == currentFortune) {
    //     QTimer::singleShot(0, this, SLOT(requestNewFortune()));
    //     return;
    // }
    // currentFortune = nextFortune;
    // statusLabel->setText(currentFortune);
    // getFortuneButton->setEnabled(true);
}

void MainWindow::xinput(QString code)
{
    int x;
    qDebug() << "xinput" << code;

    code = code.trimmed();
    if (code.startsWith("#") || (x = code.indexOf(':', 0)) < 0)
        return;

    if (code[x+1] == '/')
    {
        openLink(QUrl(code)); // view->load(QUrl(code));
        return;
    }

    QString tag;
    tag.append(code.midRef(0, x));
    code.remove(0, x+1);

    tag = tag.trimmed();
    code = code.trimmed();

    if (code.isEmpty())
        return;

    if (tag == "js")
    {
        view->page()->mainFrame()->evaluateJavaScript(code);
    }
    else if (tag == "wm")
    {
        if (code == "active")
        {
            this->setWindowState((windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
            this->show();
        }
        else if (code == "maximize")
        {
            this->setWindowState((windowState() & ~Qt::WindowMinimized) | Qt::WindowActive | Qt::WindowMaximized);
          // Qt::WindowMaximized & windowState() == 0
            this->showMaximized();
        }
        else if (code == "minimize")
        {
          // Qt::WindowMinimized & windowState() == 0
            this->showMinimized();
        }
        else if (code == "close")
        {
            this->close();
        }
    }
}

