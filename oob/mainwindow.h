#include <QtGui>
#include <QtNetwork>

class QWebView;
QT_BEGIN_NAMESPACE
class QLineEdit;
QT_END_NAMESPACE

//! [1]
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(const QUrl& url);

protected slots:

    void adjustLocation();
    void changeLocation();
    void adjustTitle();
    void setProgress(int p);
    void finishLoading(bool);
    void startLoading();

    void viewSource();
    void slotSourceDownloaded();

    void highlightAllLinks();
    void rotateImages(bool invert);
    void removeGifImages();
    void removeInlineFrames();
    void removeObjectElements();
    void removeEmbeddedElements();

    void tcpSocketError(QAbstractSocket::SocketError socketError);
    void readSocket();
    void reConnectSocket();

    void xinput(QString code);

    void openLink(const QUrl& url);
    void reOpenLink();

private:

    QString jQuery;
    QWebView *view;
    QLineEdit *locationEdit;
    QAction *rotateAction;
    int progress;
    QTcpSocket *tcpSocket;
    QString readbuf;
    QUrl url_;
    QTimer *timer_;
//! [1]
};
