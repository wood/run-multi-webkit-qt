#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <set>
#include <fstream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/trim.hpp>
// #include <boost/algorithm/string/find.hpp>
#include <boost/algorithm/string/classification.hpp>

// #include "chat_message.hpp"

using boost::asio::ip::tcp;

class client // : public chat_participant, public boost::enable_shared_from_this<browser>
{
public:
    boost::asio::streambuf linebuf;
    tcp::socket socket;
    std::string script;

    client(boost::asio::io_service& io_service) : socket(io_service) {}
};

typedef boost::shared_ptr<client> client_ptr;

//----------------------------------------------------------------------
struct umap {
    std::string cfgurlp;
    std::string cfgstatus;
    std::string cfgscript;
};

class server
{
public:
    server(const char* umapfile, const tcp::endpoint& endpoint, boost::asio::io_service& io_service)
        : acceptor_(io_service, endpoint), io_service_(io_service)
    {
        acceptor_.set_option(tcp::acceptor::reuse_address(true));

        std::string line;
        boost::regex ex("^\\s*(\\S+)\\s+(\\S+)\\s+(.+)$");
        std::ifstream inf(umapfile);

        while (getline(inf, line)) {
            auto it = std::find_if(line.begin(), line.end(), !boost::algorithm::is_space());
            if (it == line.end() || *it == '#')
                continue;

            boost::smatch what;
            if(boost::regex_match(line, what, ex))
            {
                umap um;
                um.cfgurlp.assign(what[1].first, what[1].second);
                um.cfgstatus.assign(what[2].first, what[2].second);
                // um.method.assign(what[3].first, what[3].second);
                um.cfgscript.assign(what[3].first, what[3].second);
                umaps_.push_back(um);
                std::clog << um.cfgurlp
                    << " & " << um.cfgstatus
                    << " & " << um.cfgscript
                    << "\n";
            }
        }

        start_accept();
    }

  void start_accept()
  {
    client_ptr cli(new client(io_service_));
    acceptor_.async_accept(cli->socket,
        boost::bind(&server::handle_accept, this, cli, boost::asio::placeholders::error));
  }

  void handle_accept(client_ptr cli, const boost::system::error_code& error)
  {
    if (!error)
    {
        boost::asio::async_read_until(cli->socket, cli->linebuf, '\n', 
                boost::bind(
                    &server::handle_readline, this, cli,
                    boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
        std::clog << cli->socket.remote_endpoint() << " acceptted\n";
    }

    start_accept();
  }

  void handle_write(client_ptr cli, const boost::system::error_code& e, std::size_t size)
  {
      if (e)
      {
          clients_.remove(cli);
          std::clog << cli->socket.remote_endpoint() << ": " << e.message() << "\n";
          return;
      }
      std::clog << cli->socket.remote_endpoint() << "<< " << size << ":" << cli->script << "\n";
  }

  bool usescript(client_ptr cli, const std::string& js)
  {
      cli->script = js; // boost::algorithm::trim_copy(js) + "\n";

      boost::algorithm::trim(cli->script);
      if (cli->script.empty()) {
          ;
          return false;
      }
      cli->script += "\n";

      boost::asio::async_write(cli->socket
              , boost::asio::buffer(cli->script.data(), cli->script.length())
              , boost::bind(&server::handle_write, this, cli, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
      // std::clog << cli->socket.remote_endpoint() << "<< " << js;
      return true;
  }

  std::list<umap>::iterator map_search(std::list<umap>::iterator begin, std::list<umap>::iterator end, const std::string& url, const std::string& status)
  {
      for (; begin != end; ++begin)
      {
          if (begin->cfgstatus == status)
          {
              boost::regex ex(begin->cfgurlp);
              boost::smatch what;
              if(boost::regex_search(url, what, ex))
              {
                  return begin;
              }
          }
      }
      // std::clog << url << " " << status << ": Not match\n";
      return end;
  }

  std::string get_script(const std::string& sc)
  {
      if (sc.length() > 5 && std::equal(sc.data(), sc.data()+5, "pipe:"))
      {
          std::string ret;
          FILE *rf = popen(sc.c_str() + 5, "r");
          if (rf) {
              char tmp[256];
              while (fgets(tmp, sizeof(tmp), rf))
                  ret += tmp;
              pclose(rf);
          }
          return ret;
      }
      return sc;
  }

  void handle_readline(client_ptr cli, const boost::system::error_code& e, std::size_t size) // void handle_readline(const boost::system::error_code& error)
  {
      if (e) {
          clients_.remove(cli);
          std::clog << cli->socket.remote_endpoint() << ": " << e.message() << "\n";
          return;
      }

      std::string url, status;

      std::istream is(&cli->linebuf);
      is >> url >> status;

      std::clog << cli->socket.remote_endpoint() << ">> " << url << "#" << status << "\n";

      if (status == "ready") {
          auto it = map_search(umaps_.begin(), umaps_.end(), url, status);
          if (it == umaps_.end()
                  || !usescript(cli, get_script(it->cfgscript)))
          {
              clients_.push_back(cli);
          }
      } else if (status == "loading") {
          auto it = std::find(clients_.begin(), clients_.end(), cli);
          if (it != clients_.end())
          {
              clients_.erase(it);
          }
          // usescript(cli, "wm: minimize\n");
          auto jt = map_search(umaps_.begin(), umaps_.end(), url, status);
          if (jt != umaps_.end())
          {
              usescript(cli, get_script(jt->cfgscript));
          }
      }

      if (!clients_.empty())
      {
          usescript(clients_.front(), "wm: active\n");
      }

      cli->linebuf.consume(cli->linebuf.size());

      boost::asio::async_read_until(cli->socket, cli->linebuf, '\n', 
              boost::bind(&server::handle_readline, this, cli, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
  }

private:
  std::list<client_ptr> clients_;
  std::list<umap> umaps_;

  tcp::acceptor acceptor_;
  boost::asio::io_service& io_service_;
};

static const int port = 9090;
static const char* urlmapfile = "urlp";

int main(int argc, char* argv[])
{
    try
    {
        if (argc == 2)
        {
            urlmapfile = argv[1];
            // std::cerr << "Usage: a.out <Url-Map> <Url-Init> <N-Client>\n";
        }

        boost::asio::io_service io_service;

        server server(urlmapfile, tcp::endpoint(tcp::v4(), port), io_service);

        io_service.run();
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}

